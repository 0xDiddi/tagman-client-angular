import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'DateFormatter'
})
export class DateFormatterPipe implements PipeTransform {

    transform(value: number, ...args: any[]): string {
        const c = new Date();
        const d = new Date(value);

        const diffSeconds = (c.getTime() - value) / 1000;
        if (diffSeconds < 120) return 'just now';

        const diffMin = Math.trunc(diffSeconds / 60);
        const minText = diffMin === 1 ? 'minute' : 'minutes';
        if (diffMin < 60) return `${diffMin} ${minText} ago`;

        const diffHours = Math.trunc(diffMin / 60);
        const hourText = diffHours === 1 ? 'hour' : 'hours';
        if (diffHours < 24) return `${diffHours} ${hourText} ago`;

        return d.toLocaleString();
    }

}
