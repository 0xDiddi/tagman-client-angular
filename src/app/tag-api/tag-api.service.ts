import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AlertService} from '../alerts/alert.service';

const API_URL = environment.backendUrl;

@Injectable({
    providedIn: 'root',
})
export class TagApiService {
    currentTags: Tag[] = [];
    currentGroups: Group[] = [];

    constructor(private http: HttpClient,
                private alerts: AlertService) {
    }

    updateTags = () => {
        return this.getTags().then((tags) => this.currentTags = tags)
            .catch(err => this.alerts.pushAlert({type: 'danger', message: 'Could not fetch tags'}, 5000));
    };

    updateGroups = () => {
        return this.getGroups().then((tags) => this.currentGroups = tags)
            .catch(err => this.alerts.pushAlert({type: 'warning', message: 'Could not fetch groups'}, 5000));
    };

    getTags = () => {
        return this.http.get<Tag[]>(`${API_URL}/tag`).toPromise();
    };

    createTag = (tag: string) => {
        return this.http.post(`${API_URL}/tag`, {name: tag},
            {withCredentials: true}
        ).toPromise().then(this.updateTags);
    };

    getGroups = () => {
        return this.http.get<Group[]>(`${API_URL}/group`).toPromise();
    };

    createGroup = (group: string) => {
        return this.http.post(`${API_URL}/group`, {name: group},
            {withCredentials: true}
        ).toPromise().then(this.updateGroups);
    };

    assignTagToGroup = (tag: string, group: string) => {
        return this.http.put(`${API_URL}/group/${group}/${tag}`, {}, {withCredentials: true}).toPromise()
            .then(this.updateGroups).then(this.updateTags);
    };
}

export interface Tag {
    name: string;
    group: string;
}

export interface Group {
    name: string;
    tags: string[];
}
