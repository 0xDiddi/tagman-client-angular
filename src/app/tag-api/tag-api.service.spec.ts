import { TestBed } from '@angular/core/testing';

import { TagApiService } from './tag-api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('TagApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TagApiService]
  }));

  it('should be created', () => {
    const service: TagApiService = TestBed.get(TagApiService);
    expect(service).toBeTruthy();
  });
});
