import { TestBed } from '@angular/core/testing';

import { PostApiService } from './post-api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('PostApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PostApiService]
  }));

  it('should be created', () => {
    const service: PostApiService = TestBed.get(PostApiService);
    expect(service).toBeTruthy();
  });
});
