import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

const API_URL = environment.backendUrl;

@Injectable({
    providedIn: 'root'
})
export class PostApiService {

    constructor(private http: HttpClient) {
    }

    getPostDetails = (hash: string) => {
        return this.http.get<PostDetails>(`${API_URL}/meme/${encodeURIComponent(hash)}`).toPromise()
            .then((details: PostDetails) => {
                // sort tags first by group and then by name within groups
                details.tags.sort((a, b) => {
                    if (a.group === b.group) {
                        return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
                    } else {
                        return a.group < b.group ? -1 : a.group > b.group ? 1 : 0;
                    }
                });
                return details;
            });
    };

    assignTags = (hash: string, tags: string[]) => {
        const promises = [];
        for (const tag of tags) {
            promises.push(this.http.put(`${API_URL}/meme/${encodeURIComponent(hash)}/${tag}`, {}, {withCredentials: true}).toPromise());
        }
        return Promise.all(promises);
    };

    unassignTags = (hash: string, tags: string[]) => {
        const promises = [];
        for (const tag of tags) {
            promises.push(this.http.delete(`${API_URL}/meme/${encodeURIComponent(hash)}/${tag}`, {withCredentials: true}).toPromise());
        }
        return Promise.all(promises);
    };

    postComment = (hash: string, text: string) => {
        return this.http.post(`${API_URL}/meme/${encodeURIComponent(hash)}/comment`, {text}, {withCredentials: true}).toPromise();
    };

    editComment = (hash: string, timestamp: number, text: string) => {
        return this.http.put(`${API_URL}/meme/${encodeURIComponent(hash)}/comment/${timestamp}`, {text}, {withCredentials: true})
            .toPromise();
    };

    deleteComment = (hash: string, timestamp: number) => {
        return this.http.delete(`${API_URL}/meme/${encodeURIComponent(hash)}/comment/${timestamp}`, {withCredentials: true}).toPromise();
    };

    imageSource = (hash: string) => {
        /*
        This needs to be url encoded twice for a reason.
        The files in the backend are saved on disk with the filename being urlencoded to enable the hash to be used as filename.
        The static access middleware in express however automatically decodes 'one layer' of the encoding, thus to find the file
        with the encoded filename, decoding it once needs to yield the encoded name, thus we encode it twice.
         */
        return `${API_URL}/meme/access/${encodeURIComponent(encodeURIComponent(hash))}`;
    };
}

interface PosterInfo {
    name: string;
    picture: string;
}

export interface PostComment {
    text: string;
    posted: number;
    modified: number | undefined;
    poster: PosterInfo;
}

export interface PostDetails {
    content_hash: string;
    uploaded: number;
    poster: PosterInfo;
    comments: PostComment[];
    tags: {
        name: string;
        group: string;
        tagged_when: number;
        tagged_by: PosterInfo;
    }[];
}
