import {Component} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AlertService} from '../alerts/alert.service';
import {Tag, TagApiService} from '../tag-api/tag-api.service';
import {GroupDialogComponent} from '../group-dialog/group-dialog.component';

@Component({
    selector: 'app-tag-dialog',
    templateUrl: './tag-dialog.component.html',
    styleUrls: ['./tag-dialog.component.sass'],
})
export class TagDialogComponent {
    allowCreation = false;
    private newTagName = '';

    excludeFromView: string[] = [];

    private filteredTagList: Tag[] = [];
    private selectedTags: string[] = [];

    allowGroupEditing = false;
    okText = 'Done';

    constructor(private activeModal: NgbActiveModal,
                private modalService: NgbModal,
                private tagService: TagApiService,
                private alerts: AlertService) {

        tagService.updateTags().then(this.updateFilteredView);
    }

    createTag() {
        this.tagService.createTag(this.newTagName)
            .then(this.updateFilteredView)
            .then(() => this.alerts.pushAlert({type: 'success', message: 'Tag created or it existed already'}, 5000))
            .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000))
            .finally(() => this.newTagName = '');
    }

    selectTag(tag: string) {
        if (this.selectedTags.indexOf(tag) >= 0) this.selectedTags.splice(this.selectedTags.indexOf(tag), 1);
        else this.selectedTags.push(tag);
    }

    private updateFilteredView = () => {
        this.filteredTagList = this.tagService.currentTags.filter(tag => this.excludeFromView.indexOf(tag.name) === -1);
    };

    selectGroupForTag(tag: Tag, evt: MouseEvent) {
        // this'll stop a click on the badge from selecting the tag
        evt.stopPropagation();

        if (!this.allowGroupEditing) return;

        const ref = this.modalService.open(GroupDialogComponent, {size: 'lg'});
        ref.componentInstance.okText = 'Assign';
        ref.componentInstance.selectedGroup = tag.group;
        ref.result.then((group: string) => {
            this.tagService.assignTagToGroup(tag.name, group)
                .then(this.updateFilteredView)
                .then(() => this.alerts.pushAlert({type: 'success', message: 'Tag was assigned to group'}, 5000))
                .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000));
        }).catch(() => {
            // the dialog was dismissed; we do nothing but catch the promise
        });
    }
}
