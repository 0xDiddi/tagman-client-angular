import {Component, ElementRef, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {PostApiService, PostComment, PostDetails} from '../post-api/post-api.service';
import {AlertService} from '../alerts/alert.service';
import {TagDialogComponent} from '../tag-dialog/tag-dialog.component';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {UserApiService} from '../user-api/user-api.service';

@Component({
    selector: 'app-detail-view',
    templateUrl: './detail-view.component.html',
    styleUrls: ['./detail-view.component.sass']
})
export class DetailViewComponent {
    private hash: string;
    private currentPost: PostDetails;

    private temp: string;

    private selectedTags: string[] = [];

    private newCommentText = '';

    private editCommentTimestamp = -1;
    private editCommentText = '';

    @ViewChild('edit_comment_modal', {static: true}) editCommentModalTemplate: ElementRef;
    editCommentModal: NgbModalRef;

    constructor(private route: ActivatedRoute,
                private modalService: NgbModal,
                private postApi: PostApiService,
                private userApi: UserApiService, // we need this to check if the current user posted a comment
                private alerts: AlertService) {
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.hash = params.get('hash');
            postApi.getPostDetails(this.hash)
                .then((details) => {
                    this.currentPost = details;
                    this.temp = `url(${this.postApi.imageSource(this.hash)})`;
                })
                .catch(() => this.alerts.pushAlert({type: 'danger', message: 'Could not load post'}, 5000));
        });
    }

    selectTag(tag: string) {
        if (this.selectedTags.indexOf(tag) >= 0) this.selectedTags.splice(this.selectedTags.indexOf(tag), 1);
        else this.selectedTags.push(tag);
    }

    openTagSelect() {
        const ref = this.modalService.open(TagDialogComponent, {size: 'lg'});
        ref.componentInstance.allowCreation = true;
        ref.componentInstance.excludeFromView = this.currentPost.tags.map(t => t.name);
        ref.componentInstance.okText = 'Add';
        ref.result.then((tags: string[]) => {
            this.postApi.assignTags(this.hash, tags)
                .then(this.refreshPost)
                .then(() => this.alerts.pushAlert({type: 'success', message: 'Tags assigned successfully'}, 5000))
                .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000));
        }).catch(() => {
            // no tags have been selected, but we need to catch the promise
        });
    }

    untagSelected() {
        this.postApi.unassignTags(this.hash, this.selectedTags)
            .then(() => this.selectedTags = [])
            .then(this.refreshPost)
            .then(() => this.alerts.pushAlert({type: 'success', message: 'Tags removed successfully'}, 5000))
            .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000));
    }

    postComment() {
        this.postApi.postComment(this.hash, this.newCommentText)
            .then(() => this.newCommentText = '')
            .then(this.refreshPost)
            .then(() => this.alerts.pushAlert({type: 'success', message: 'Comment posted successfully'}, 5000))
            .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000));
    }

    editComment(comment: PostComment) {
        this.editCommentText = comment.text;
        this.editCommentTimestamp = comment.posted;
        this.editCommentModal  = this.modalService.open(this.editCommentModalTemplate);
    }

    finalizeEditComment() {
        this.postApi.editComment(this.hash, this.editCommentTimestamp, this.editCommentText)
            .then(() => {
                this.editCommentText = '';
                this.editCommentModal.close();
            })
            .then(this.refreshPost)
            .then(() => this.alerts.pushAlert({type: 'success', message: 'Comment edited successfully'}, 5000))
            .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000));
    }

    deleteComment(ts: number) {
        this.postApi.deleteComment(this.hash, ts)
            .then(this.refreshPost)
            .then(() => this.alerts.pushAlert({type: 'success', message: 'Comment deleted successfully'}, 5000))
            .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000));
    }

    private refreshPost = () => {
        return this.postApi.getPostDetails(this.hash).then((post: PostDetails) => this.currentPost = post);
    };
}
