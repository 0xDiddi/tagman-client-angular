import {Injectable} from '@angular/core';

interface Alert {
    id?: number;
    type: 'success' | 'info' | 'warning' | 'danger' | 'primary' | 'secondary' | 'light' | 'dark';
    message: string;
}

@Injectable({
    providedIn: 'root',
})
export class AlertService {
    private alertNextId = 0;
    alerts: Alert[] = [];

    pushAlert = (alert: Alert, timeout?: number) => {
        alert.id = this.alertNextId++;
        this.alerts.push(alert);
        if (!!timeout) {
            setTimeout(() => this.delete(alert), timeout);
        }
    };

    delete = (alert: Alert) => {
        this.alerts.splice(this.alerts.indexOf(alert), 1);
    };

    deleteAt = (idx: number) => {
        this.alerts.splice(idx, 1);
    };
}
