import {Component} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Group, TagApiService} from '../tag-api/tag-api.service';
import {AlertService} from '../alerts/alert.service';
import {TagDialogComponent} from '../tag-dialog/tag-dialog.component';

@Component({
    selector: 'app-group-dialog',
    templateUrl: './group-dialog.component.html',
    styleUrls: ['./group-dialog.component.sass']
})
export class GroupDialogComponent {
    // selectedGroup is public to allow pre-selecting a group
    selectedGroup = '';
    okText = 'Done';

    allowTagAssigning = false;
    private newGroupName = '';
    private copyOfGroups: Group[] = [];

    constructor(private activeModal: NgbActiveModal,
                private modalService: NgbModal,
                private tagService: TagApiService,
                private alerts: AlertService) {

        tagService.updateGroups().then(this.updateGroups);
    }

    createGroup() {
        this.tagService.createGroup(this.newGroupName)
            .then(this.updateGroups)
            .then(() => this.alerts.pushAlert({type: 'success', message: 'Group created or it existed already'}, 5000))
            .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000))
            .finally(() => this.newGroupName = '');
    }

    private updateGroups = () => {
        // todo: I want to have a search bar here, and we need to filter the list for that here
        this.copyOfGroups = this.tagService.currentGroups;
    };

    assignTagsToGroup(group: Group, evt: MouseEvent) {
        evt.stopPropagation();

        if (!this.allowTagAssigning) return;

        const ref = this.modalService.open(TagDialogComponent, {size: 'lg'});
        ref.componentInstance.okText = 'Assign';
        ref.componentInstance.excludeFromView = group.tags;
        ref.result.then((tags: string[]) => {
            const prom = [];
            for (const tag of tags) prom.push(this.tagService.assignTagToGroup(tag, group.name));
            Promise.all(prom)
                .then(this.updateGroups)
                .then(() => this.alerts.pushAlert({type: 'success', message: 'Tags assigned successfully'}, 5000))
                .catch(err => this.alerts.pushAlert({type: 'danger', message: err.error.message}, 5000));
        }).catch(() => {
            // the dialog was dismissed; we do nothing but catch the promise
        });
    }
}
