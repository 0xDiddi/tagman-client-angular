import {AppPage} from './app.po';
import {browser, logging} from 'protractor';

describe('workspace-project App', () => {
    let page: AppPage;

    beforeEach(() => {
        page = new AppPage();
    });

    // TODO: the page makes a request immediately when loading to check whether
    //  the user is logged in/his token is still valid.
    //  this fails because the backend is unavailable.
    //  Problem: there doesn't seem to exist a decent method to mock that.
    /*
    it('should display welcome message', () => {
        browser.addMockModule('userApiService', () => {

        });

        page.navigateTo();
        expect(page.getTitleText()).toEqual('Tagman');
    });
    */

    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
